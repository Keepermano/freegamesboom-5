importScripts('https://www.gstatic.com/firebasejs/6.4.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/6.4.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '795521926689'
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
  const notificationTitle = payload.data.title;
  const notificationOptions = {
    body: payload.data.body,
    icon: payload.data.icon,
    click_action: payload.data.click_action
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});