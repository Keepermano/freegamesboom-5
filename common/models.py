import os

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.db.models.signals import post_delete, pre_save
from django.dispatch import receiver

from ckeditor_uploader.fields import RichTextUploadingField
from sorl.thumbnail import ImageField

from common.fields import CharTextField
from common.utils import TranslationFieldDescriptor, upload_dir


def ___(val):
    return val


class PublicManager(models.Manager):

    def get_queryset(self):
        return super().get_queryset().filter(active=True)


settings_text_fields = [
    'title',
    'meta_desc',
    'meta_keywords',
    'bottom_text',
    'tm_title_game',
    'tm_title_category',
    'tm_desc_game',
    'tm_desc_category',
    'tm_title_tag',
    'tm_desc_tag',
    'tl_desc',
]


class SettingsTranslation(models.Model):
    text_block_code = models.CharField(max_length=100, choices=[(v, v) for v in settings_text_fields])
    language_code = models.CharField(max_length=10, choices=settings.LANGUAGES)
    text = models.TextField()

    def __str__(self):
        return f"Text block code: '{self.text_block_code}', lang: '{self.language_code}'"


class Settings(models.Model):
    email = models.EmailField(verbose_name=___('Email'))
    count_items = models.PositiveIntegerField(verbose_name='Objects on pagination', default=21)
    count_tags = models.PositiveIntegerField(verbose_name='Tags on page', default=20)
    background = ImageField(verbose_name=___('Background'), upload_to=upload_dir, blank=True)
    logo = ImageField(verbose_name=___('Logo'), upload_to=upload_dir, blank=True)
    footer_social = RichTextUploadingField(verbose_name=___('Footer social links'), blank=True, null=True)

    title = TranslationFieldDescriptor(SettingsTranslation)
    meta_desc = TranslationFieldDescriptor(SettingsTranslation)
    meta_keywords = TranslationFieldDescriptor(SettingsTranslation)
    bottom_text = TranslationFieldDescriptor(SettingsTranslation)
    tm_title_game = TranslationFieldDescriptor(SettingsTranslation)
    tm_title_category = TranslationFieldDescriptor(SettingsTranslation)
    tm_desc_game = TranslationFieldDescriptor(SettingsTranslation)
    tm_desc_category = TranslationFieldDescriptor(SettingsTranslation)
    tm_title_tag = TranslationFieldDescriptor(SettingsTranslation)
    tm_desc_tag = TranslationFieldDescriptor(SettingsTranslation)
    tl_desc = TranslationFieldDescriptor(SettingsTranslation)

    # tm_title_game = models.TextField(
    #     blank=True,
    #     help_text='[name], [name2_seo], [name_seo_en], [category]'
    # )
    # tm_desc_game = models.TextField(
    #     blank=True,
    #     help_text='[name],[name2_seo], [name_seo_en], [category]'
    # )
    # tm_title_category = models.TextField(
    #     blank=True,
    #     help_text='[name],[name2_seo], [name_seo_en], [total_games]'
    # )
    # tm_desc_category = models.TextField(
    #     blank=True,
    #     help_text='[name],[name2_seo], [name_seo_en], [total_games]'
    # )
    # tm_title_tag = models.TextField(
    #     blank=True,
    #     help_text='[name],[name2_seo], [name_seo_en], [total_games]'
    # )
    # tm_desc_tag = models.TextField(
    #     blank=True,
    #     help_text='[name],[name2_seo], [name_seo_en], [total_games]'
    # )
    # tl_desc = models.TextField(
    #     verbose_name='Тех.описание игры',
    #     blank=True,
    #     help_text='[name], [name_en], [total_plays], [rating], [total_likes], '
    #               '<br>[IFMOBILE]text[ELSE]another text[ENDIF]'
    #               '<br>[IFFLASH]text[ELSE]another text[ENDIF]'
    # )

    class Meta:
        verbose_name = verbose_name_plural = ___('Settings')

    def save(self, *args, **kwargs):
        self.pk = 1
        super(Settings, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        pass

    @classmethod
    def load(cls):
        obj, created = cls.objects.get_or_create(pk=1)
        return obj

    def __str__(self):
        return 'General site settings'


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


class SiteFile(models.Model):
    settings = models.ForeignKey(Settings, related_name='site_files', on_delete=models.CASCADE)
    file = models.FileField(storage=OverwriteStorage())

    class Meta:
        verbose_name = ___('File')
        verbose_name_plural = ___('Files')

    def __str__(self):
        return self.file.url


@receiver(post_delete, sender=SiteFile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)


@receiver(pre_save, sender=SiteFile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    old_file = instance.file

    new_file = instance.file
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Counter(models.Model):
    COUNTER_TYPE = (
        (1, 'top'),
        (2, 'bottom')
    )
    settings = models.ForeignKey(Settings, related_name='+', on_delete=models.CASCADE)
    title = models.CharField(verbose_name=___('Title'), max_length=50, blank=True)
    type = models.IntegerField(verbose_name=___('Place'), choices=COUNTER_TYPE, default=1)
    content = models.TextField(verbose_name=___('Code'))
    active = models.BooleanField(default=True, verbose_name=___('Active'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = ___('Counter')
        verbose_name_plural = ___('Counters')


class MenuAbstract(models.Model):
    title = CharTextField(verbose_name=___('Title'))
    url = models.CharField(verbose_name=___('URL'), max_length=100)
    position = models.IntegerField(verbose_name=___('Position'), default=100)

    class Meta:
        abstract = True
        verbose_name = ___('Menu element')
        verbose_name_plural = ___('Menu elements')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return self.url


class MenuHead(MenuAbstract):
    class Meta:
        verbose_name = ___('Menu header')
        verbose_name_plural = ___('Menu header')


class MenuFooter(MenuAbstract):
    class Meta:
        verbose_name = ___('Menu footer')
        verbose_name_plural = ___('Menu footer')


class MenuButton(MenuAbstract):
    icon = ImageField(verbose_name=___('Icon'), upload_to=upload_dir, blank=True)

    class Meta:
        verbose_name = ___('Menu button')
        verbose_name_plural = ___('Menu button')


class Banner(models.Model):
    TYPES = (
        (1, 'top'),
        (2, 'right'),
    )
    type = models.IntegerField(choices=TYPES, default=1, unique=True)
    code = models.TextField()
    active = models.BooleanField(default=True, verbose_name=___('Active'))

    objects = models.Manager()
    published = PublicManager()

    def __str__(self):
        return self.get_type_display()
