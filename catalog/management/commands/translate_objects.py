# -*- coding: utf-8 -*-
import requests
from django.db.models import Q
from tqdm import tqdm
from django.conf import settings
from django.core.management.base import BaseCommand
from catalog.models import Game, Category, Tag, TranslateLanguage, GAMES_TEXT_FIELDS, GamesTranslation
from catalog.translation import GameTranslationOptions, TagTranslationOptions, CategoryTranslationOptions


class Command(BaseCommand):

    def handle(self, *args, **options):
        models = (
            (Category, CategoryTranslationOptions.fields),
            (Tag, TagTranslationOptions.fields),
            (Game, GameTranslationOptions.fields),
        )
        from_rus_fields = []
        languages = TranslateLanguage.objects.filter(processed=False, active=True)
        yandex_game_over = False

        for lang in languages:
            lang_slug = lang.slug.replace('-', '_')
            yandex_lang_out = lang_slug
            if yandex_lang_out == 'zh_hans':
                yandex_lang_out = 'zh'
            # для каждого из не отработанных языков:
            for item in tqdm(models, disable=False):
                for obj in tqdm(item[0].objects.all().order_by('-id'), disable=False):
                    for field_name in item[1]:
                        field = getattr(obj, '%s_%s' % (field_name, 'ru' if field_name in from_rus_fields else 'en'))
                        if field:
                            update_field = getattr(obj, '%s_%s' % (field_name, lang_slug))
                            if not update_field:
                                translated = translate_from_yandex(
                                    [field],
                                    'ru' if field_name in from_rus_fields else 'en',
                                    yandex_lang_out
                                )
                                if translated:
                                    setattr(obj, '%s_%s' % (field_name, lang_slug), translated[0])
                                    obj.save()
                                else:
                                    return

            for field in tqdm(GAMES_TEXT_FIELDS, disable=False):
                objects_to_create = []
                translated_games_ids = list(GamesTranslation.objects \
                    .filter(text_block_code=field, language_code=lang_slug) \
                    .values_list('game_id', flat=True))
                english_translations = GamesTranslation.objects \
                    .filter(language_code='en', text_block_code=field) \
                    .exclude(Q(game_id__in=translated_games_ids)
                             | Q(text__isnull=True)
                             | Q(text='')
                             ) \
                    .values_list('game_id', 'text')
                for game_id, text in tqdm(english_translations, disable=False):
                    if text:
                        translated = translate_from_yandex(
                            [text],
                            'en',
                            yandex_lang_out
                        )
                        if translated:
                            objects_to_create.append(GamesTranslation(
                                text_block_code=field,
                                language_code=lang_slug,
                                text=translated[0],
                                game_id=game_id
                            ))
                        else:
                            yandex_game_over = True
                            break
                GamesTranslation.objects.bulk_create(objects_to_create, ignore_conflicts=True)
                if yandex_game_over:
                    return

            lang.processed = True
            lang.save()


def translate_from_yandex(texts, lang_in, lang_out):
    r = requests.post('https://translate.api.cloud.yandex.net/translate/v2/translate', json={
        'folder_id': 'b1gp7kn6p1c17lpdnibp',
        'texts': texts,
        'targetLanguageCode': lang_out,
        'sourceLanguageCode': lang_in,
    }, headers={
        'Content-Type': 'application/json',
        'Authorization': 'Api-Key {}'.format(settings.YANDEX_API_KEY)
    })
    if r.status_code == 200:
        return [translation['text'] for translation in r.json()['translations']]
    else:
        print(r.json())
        return
