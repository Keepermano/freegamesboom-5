from modeltranslation.translator import translator, TranslationOptions
from .models import Tag, Category, Game, GameVideo, Block


class TagTranslationOptions(TranslationOptions):
    fields = (
        'name',
        'name2_seo',
        'h1',
        'text_link',
        'description',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'seo_top',
        'alt',
        'title'
    )


class CategoryTranslationOptions(TranslationOptions):
    fields = (
        'name',
        'name2_seo',
        'h1',
        'text_link',
        'description',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'seo_top'
    )


class GameTranslationOptions(TranslationOptions):
    fields = (
        'name',
        'name2_seo',


        # 'title',
        # 'h1',
        # 'description',
        # 'control',
        # 'video',
        # 'awards',
        # 'meta_title',
        # 'meta_desc',
        # 'meta_keywords',
    )


class GameVideoOptions(TranslationOptions):
    fields = ('code', )


class BlockOptions(TranslationOptions):
    fields = ('name', )


translator.register(Tag, TagTranslationOptions)
translator.register(Category, CategoryTranslationOptions)
translator.register(Game, GameTranslationOptions)
translator.register(GameVideo, GameVideoOptions)
translator.register(Block, BlockOptions)
