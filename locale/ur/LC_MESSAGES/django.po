# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-14 17:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: catalog/fields.py:23
msgid "Filetype not supported."
msgstr "فائل ٹائپ تعاون یافتہ نہیں ہے۔"

#: catalog/templates/catalog/base.html:71
msgid "free games BOOM"
msgstr " BOOM مفت کھیل"

#: catalog/templates/catalog/base.html:78
#: catalog/templates/catalog/base.html:133
msgid "Find more 100000 games"
msgstr "مزید 100000 گیمز تلاش کریں"

#: catalog/templates/catalog/base.html:79
#: catalog/templates/catalog/base.html:134
#: catalog/templates/catalog/search.html:7
msgid "Search"
msgstr "تلاش کریں"

#: catalog/templates/catalog/base.html:97
#: catalog/templates/catalog/inclusions/sort_block.html:6
msgid "New"
msgstr "نئی"

#: catalog/templates/catalog/base.html:103
msgid "Popular"
msgstr "مقبول"

#: catalog/templates/catalog/base.html:114
msgid "Favourite <br>games"
msgstr "پسندیدہ <br> کھیل"

#: catalog/templates/catalog/base.html:125
msgid "Last <br>played"
msgstr "آخری <br> ادا کیا"

#: catalog/templates/catalog/category.html:95
msgid "Information"
msgstr "معلومات"

#: catalog/templates/catalog/game.html:90
msgid "Category"
msgstr "قسم"

#: catalog/templates/catalog/game.html:105
msgid "Game"
msgstr "کھیل"

#: catalog/templates/catalog/game.html:131
#: catalog/templates/catalog/game.html:133
msgid "PLAY"
msgstr "کھیلیں"

#: catalog/templates/catalog/game.html:142
msgid "This game is not available on mobile."
msgstr "یہ گیم موبائل پر دستیاب نہیں ہے۔"

#: catalog/templates/catalog/game.html:174
msgid "Controls"
msgstr "کنٹرولز"

#: catalog/templates/catalog/game.html:181
msgid "Video"
msgstr "ویڈیو"

#: catalog/templates/catalog/inclusions/_include_game_list.html:19
msgid "Play!"
msgstr "کھیلیں!"

#: catalog/templates/catalog/inclusions/category_menu.html:9
msgid "TOP CATEGORIES"
msgstr "سب سے اوپر کیٹیگریز"

#: catalog/templates/catalog/inclusions/game_block.html:5
msgid "votes"
msgstr "ووٹ"

#: catalog/templates/catalog/inclusions/game_block.html:11
msgid "Doesn’t  <br>work?"
msgstr "<br> کام نہیں کرتا؟"

#: catalog/templates/catalog/inclusions/game_block.html:17 catalog/views.py:226
msgid "Added"
msgstr "شامل"

#: catalog/templates/catalog/inclusions/game_block.html:17
msgid "Add to <br>favorites"
msgstr "<br> پسندیدہ میں شامل کریں"

#: catalog/templates/catalog/inclusions/game_block.html:35
msgid "Share"
msgstr "بانٹیں"

#: catalog/templates/catalog/inclusions/game_block.html:41
msgid "Fullscreen"
msgstr "مکمل اسکرین یا بڑی اسکرین"

#: catalog/templates/catalog/inclusions/popup-games.html:7
msgid "Clear list"
msgstr "واضح فہرست"

#: catalog/templates/catalog/inclusions/sort_block.html:3
msgid "Sort by"
msgstr "کے لحاظ سے ترتیب دیں"

#: catalog/templates/catalog/inclusions/sort_block.html:3
#: catalog/templates/catalog/inclusions/sort_block.html:5
msgid "Most Popular"
msgstr "سب سے زیادہ مقبول"

#: catalog/templates/catalog/inclusions/sort_block.html:7
msgid "A-Z"
msgstr "A-Z"

#: catalog/templates/catalog/tags.html:6
msgid "Tags"
msgstr "ٹیگز"

#: catalog/templates/catalog/tags.html:11
msgid "Popular tags"
msgstr "مقبول ٹیگ"

#: catalog/templatetags/catalog_tags.py:45
msgid "MOST POPULAR GAMES"
msgstr "انتہائی مشہور کھیل"

#: catalog/templatetags/catalog_tags.py:64
msgid "POPULAR TAGS"
msgstr "مشہور ٹیگز"

#: catalog/templatetags/catalog_tags.py:87
msgid "MOST POPULAR"
msgstr "سب سے زیادہ مقبول"

#: catalog/templatetags/catalog_tags.py:156
msgid "FGM RECOMMENDED!"
msgstr "FGM سفارش کی!"

#: catalog/templatetags/catalog_tags.py:160
#, python-format
msgid "MORE %(category)s"
msgstr "مزید %(category)s"

#: catalog/templatetags/catalog_tags.py:187
msgid "LAST PLAYED"
msgstr "آخری کھیلے گئے"

#: catalog/templatetags/catalog_tags.py:282 catalog/views.py:71
msgid "New games"
msgstr "نئے کھیل"

#: catalog/views.py:70
msgid "New games - Free Games Boom.com"
msgstr "نئے کھیل - Free Games Boom.com"

#: catalog/views.py:74
msgid "Popular games - Free Games Boom.com"
msgstr "مشہور کھیل - Free Games Boom.com"

#: catalog/views.py:75
msgid "Popular games"
msgstr "مشہور کھیل"

#: catalog/views.py:99
#, fuzzy, python-format
#| msgid "Popular games"
msgid "Similar %s games"
msgstr "مشہور کھیل"

#: catalog/views.py:191
msgid "Favourite games"
msgstr "پسندیدہ کھیل"

#: catalog/views.py:194
msgid "Last Played"
msgstr "آخری کھیلے گئے"

#: flatpages/apps.py:7
msgid "Flat Pages"
msgstr "فلیٹ صفحات"

#: flatpages/models.py:12
msgid "Title"
msgstr "عنوان"

#: flatpages/models.py:14
msgid "Text"
msgstr "متن"

#: flatpages/models.py:15
msgid "Active"
msgstr "فعال"

#: flatpages/models.py:16
msgid "Modified"
msgstr "ترمیم شدہ"

#: flatpages/models.py:19
msgid "Static pages"
msgstr "جامد صفحات"

#: flatpages/models.py:20
msgid "Static page"
msgstr "جامد صفحہ"

#: freegamesboom/settings.py:155
msgid "Afrikaans"
msgstr "افریقی"

#: freegamesboom/settings.py:156
msgid "Arabic"
msgstr "عربی"

#: freegamesboom/settings.py:157
msgid "Azerbaijani"
msgstr "آزربائیجانی"

#: freegamesboom/settings.py:158
msgid "Bulgarian"
msgstr "بلغاریائی"

#: freegamesboom/settings.py:159
msgid "Belarusian"
msgstr "بیلاروس"

#: freegamesboom/settings.py:160
msgid "Bengali"
msgstr "بنگالی"

#: freegamesboom/settings.py:161
msgid "Bosnian"
msgstr "بوسنیائی"

#: freegamesboom/settings.py:162
msgid "Catalan"
msgstr "کاتالان"

#: freegamesboom/settings.py:163
msgid "Czech"
msgstr "چیک"

#: freegamesboom/settings.py:164
msgid "Welsh"
msgstr "ویلش"

#: freegamesboom/settings.py:165
msgid "Danish"
msgstr "دانش"

#: freegamesboom/settings.py:166
msgid "German"
msgstr "جرمن"

#: freegamesboom/settings.py:167
msgid "Greek"
msgstr "یونانی"

#: freegamesboom/settings.py:168
msgid "English"
msgstr "انگریزی"

#: freegamesboom/settings.py:169
msgid "Esperanto"
msgstr "ایسپرانٹو"

#: freegamesboom/settings.py:170
msgid "Spanish"
msgstr "ہسپانوی"

#: freegamesboom/settings.py:171
msgid "Estonian"
msgstr "اسٹونین"

#: freegamesboom/settings.py:172
msgid "Finnish"
msgstr "فینیش"

#: freegamesboom/settings.py:173
msgid "French"
msgstr "فرانسیسی"

#: freegamesboom/settings.py:174
msgid "Irish"
msgstr "آئرش"

#: freegamesboom/settings.py:175
msgid "Galician"
msgstr "گالیشین"

#: freegamesboom/settings.py:176
msgid "Hebrew"
msgstr "عبرانی"

#: freegamesboom/settings.py:177
msgid "Hindi"
msgstr "نہیں"

#: freegamesboom/settings.py:178
msgid "Croatian"
msgstr "کروشین"

#: freegamesboom/settings.py:179
msgid "Hungarian"
msgstr "ہنگری"

#: freegamesboom/settings.py:180
msgid "Armenian"
msgstr "آرمینیائی"

#: freegamesboom/settings.py:182
msgid "Italian"
msgstr "اطالوی"

#: freegamesboom/settings.py:183
msgid "Japanese"
msgstr "جاپانی"

#: freegamesboom/settings.py:184
msgid "Georgian"
msgstr "جارجیائی"

#: freegamesboom/settings.py:186
msgid "Kazakh"
msgstr "قازق"

#: freegamesboom/settings.py:187
msgid "Khmer"
msgstr "خمیر"

#: freegamesboom/settings.py:188
msgid "Kannada"
msgstr "کناڈا"

#: freegamesboom/settings.py:189
msgid "Korean"
msgstr "کورین"

#: freegamesboom/settings.py:190
msgid "Luxembourgish"
msgstr "لکسمبرگ"

#: freegamesboom/settings.py:191
msgid "Lithuanian"
msgstr "لتھوانیائی"

#: freegamesboom/settings.py:192
msgid "Latvian"
msgstr "لیٹوین"

#: freegamesboom/settings.py:193
msgid "Macedonian"
msgstr "مقدونیائی"

#: freegamesboom/settings.py:194
msgid "Malayalam"
msgstr "ملیالم"

#: freegamesboom/settings.py:195
msgid "Mongolian"
msgstr "منگولیا"

#: freegamesboom/settings.py:196
msgid "Marathi"
msgstr "مراٹھی"

#: freegamesboom/settings.py:197
msgid "Burmese"
msgstr "برمی"

#: freegamesboom/settings.py:198
msgid "Nepali"
msgstr "نیپالی"

#: freegamesboom/settings.py:199
msgid "Dutch"
msgstr "ڈچ"

#: freegamesboom/settings.py:200
msgid "Ossetic"
msgstr "اوسیٹک"

#: freegamesboom/settings.py:201
msgid "Punjabi"
msgstr "پنجابی"

#: freegamesboom/settings.py:202
msgid "Polish"
msgstr "پولش"

#: freegamesboom/settings.py:203
msgid "Portuguese"
msgstr "پرتگالی"

#: freegamesboom/settings.py:204
msgid "Romanian"
msgstr "رومانیہ"

#: freegamesboom/settings.py:205
msgid "Russian"
msgstr "روسی"

#: freegamesboom/settings.py:206
msgid "Slovak"
msgstr "سلوواک"

#: freegamesboom/settings.py:207
msgid "Slovenian"
msgstr "سلووینیائی"

#: freegamesboom/settings.py:208
msgid "Albanian"
msgstr "البانی"

#: freegamesboom/settings.py:209
msgid "Serbian"
msgstr "سربیا"

#: freegamesboom/settings.py:210
msgid "Swedish"
msgstr "سویڈش"

#: freegamesboom/settings.py:211
msgid "Swahili"
msgstr "انگریزی"

#: freegamesboom/settings.py:212
msgid "Tamil"
msgstr "تمل"

#: freegamesboom/settings.py:213
msgid "Telugu"
msgstr "تیلگو"

#: freegamesboom/settings.py:214
msgid "Thai"
msgstr "تھائی"

#: freegamesboom/settings.py:215
msgid "Turkish"
msgstr "ترکی"

#: freegamesboom/settings.py:216
msgid "Tatar"
msgstr "تاتار"

#: freegamesboom/settings.py:218
msgid "Ukrainian"
msgstr "یوکرائن"

#: freegamesboom/settings.py:219
msgid "Urdu"
msgstr "اردو"

#: freegamesboom/settings.py:220
msgid "Vietnamese"
msgstr "ویتنامی"

#: freegamesboom/settings.py:221
msgid "Simplified Chinese"
msgstr "آسان چینی"

#: templates/404.html:18
msgid "404 Error"
msgstr "404 خرابی"

#: templates/404.html:19
msgid "Oops. This page does not exist."
msgstr "افوہ۔ یہ صفحہ موجود نہیں ہے۔"

#: templates/el_pagination/show_more.html:5
msgid "more"
msgstr "مزید"

#: templates/el_pagination/show_pages.html:6
msgid "All"
msgstr "سب"

#~ msgid "Indonesian"
#~ msgstr "انڈونیشی"

#~ msgid "Kabyle"
#~ msgstr "کابیل"

#~ msgid "Udmurt"
#~ msgstr "اڈمورٹ"
